import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';
import { DatePickerModalPage } from './date-picker-modal.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
  ],
  declarations: [DatePickerModalPage],
  entryComponents: [DatePickerModalPage]
})
export class DatePickerModalPageModule {}
