import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { DatePickerModalPage } from './date-picker-modal.page';

describe('DatePickerModalPage', () => {
  let component: DatePickerModalPage;
  let fixture: ComponentFixture<DatePickerModalPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DatePickerModalPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(DatePickerModalPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
