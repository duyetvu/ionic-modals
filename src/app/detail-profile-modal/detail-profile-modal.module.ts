import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { DetailProfileModalPage } from './detail-profile-modal.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
  ],
  declarations: [DetailProfileModalPage],
  entryComponents: [DetailProfileModalPage]
})
export class DetailProfileModalPageModule {}
