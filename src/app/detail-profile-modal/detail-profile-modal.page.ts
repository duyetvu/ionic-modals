import { Component, OnInit } from '@angular/core';
import { ViewController } from '@ionic/core';
import { ModalController } from '@ionic/angular';
import {DatePickerModalPage } from '../date-picker-modal/date-picker-modal.page';

@Component({
  selector: 'app-detail-profile-modal',
  templateUrl: './detail-profile-modal.page.html',
  styleUrls: ['./detail-profile-modal.page.scss'],
})
export class DetailProfileModalPage implements OnInit {

  constructor(private modalCtrl: ModalController) { }

  ngOnInit() {
  }

  async displayDatepicker(): Promise<void> {
    const accountListModal = await this.modalCtrl.create({
      component: DatePickerModalPage
    });
    return await accountListModal.present();
  }

  dismiss() {
    this.modalCtrl.dismiss()
  }

}
