import { Component, OnInit } from '@angular/core';
import { ModalController, NavController } from '@ionic/angular';
import { DetailProfileModalPage } from '../detail-profile-modal/detail-profile-modal.page';
import { Router } from '@angular/router';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.page.html',
  styleUrls: ['./profile.page.scss'],
})
export class ProfilePage implements OnInit {

  constructor(private modalCtrl: ModalController, private router: Router, private navCtr: NavController) { }

  ngOnInit() {
  }

  async displayProfileDetail(): Promise<void> {
    const accountListModal = await this.modalCtrl.create({
      component: DetailProfileModalPage
    });
    return await accountListModal.present();
  }

  goToHome() {
    //this.router.navigate(['/home'])
    this.navCtr.back()
    //this.navCtr.pop()
  }

}
